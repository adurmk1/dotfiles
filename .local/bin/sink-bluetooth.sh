#!/bin/sh

bluetooth=$(pamixer --list-sinks | grep -e 'bluez' | awk '{print $2}' | sed 's/\"//g' )
pactl set-default-sink $bluetooth
dunstify -i multimedia-volume-control -h string:x-dunst-stack-tag:sink 'Sound Output' 'Bluetooth'
echo 'Sink: Bluetooth'
