#!/bin/sh

xrandr --output DisplayPort-0 --primary --mode 2560x1440 --pos 1920x365 --rotate normal --rate 165.00 \
--output DisplayPort-1 --mode 1920x1080 --pos 0x545 --rotate normal --rate 164.92 \
--output HDMI-A-0 --mode 1920x1080 --pos 4480x0 --rotate left --rate 74.97 \
--output DisplayPort-2 --mode 1920x1080 --pos 5560x0 --rotate right --rate 74.97
