#!/bin/sh

polybar_theme_dir="$HOME/.config/polybar/themes"
rofi_theme_dir="$HOME/.config/rofi/themes"
xmonad_config="$HOME/.config/xmonad/xmonad.hs"
zathura_config="$HOME/.config/zathura/zathurarc"
red='"#ff6c6b"'
yellow='"#ecbe7b"'
green='"#98be65"'
blue='"#51afef"'
magenta='"#c678dd"'

if [ $1 = 'red' ]; then
   ln -sfT "$polybar_theme_dir/doom-one-red.ini" "$polybar_theme_dir/theme.ini"
   ln -sfT "$rofi_theme_dir/doom-one-red.rasi" "$rofi_theme_dir/theme.rasi"
   color=$red
elif [ $1 = 'yellow' ]; then
   ln -sfT "$polybar_theme_dir/doom-one-yellow.ini" "$polybar_theme_dir/theme.ini"
   ln -sfT "$rofi_theme_dir/doom-one-yellow.rasi" "$rofi_theme_dir/theme.rasi"
   color=$yellow
elif [ $1 = 'green' ]; then
   ln -sfT "$polybar_theme_dir/doom-one-green.ini" "$polybar_theme_dir/theme.ini"
   ln -sfT "$rofi_theme_dir/doom-one-green.rasi" "$rofi_theme_dir/theme.rasi"
   color=$green
elif [ $1 = 'blue' ]; then
   ln -sfT "$polybar_theme_dir/doom-one-blue.ini" "$polybar_theme_dir/theme.ini"
   ln -sfT "$rofi_theme_dir/doom-one-blue.rasi" "$rofi_theme_dir/theme.rasi"
   color=$blue
elif [ $1 = 'magenta' ]; then
   ln -sfT "$polybar_theme_dir/doom-one-magenta.ini" "$polybar_theme_dir/theme.ini"
   ln -sfT "$rofi_theme_dir/doom-one-magenta.rasi" "$rofi_theme_dir/theme.rasi"
   color=$magenta
else
    echo "There is no theme named $1!"
    exit 1
fi
sed -i "s/\(myFocusedBorderColor = \).*/\1$color/" "$xmonad_config"
sed -i "s/\(highlight-color \).*/\1$color/" "$zathura_config"
sed -i "s/\(highlight-active-color \).*/\1$color/" "$zathura_config"
sed -i "s/\(completion-highlight-bg \).*/\1$color/" "$zathura_config"
sed -i "s/\(notification-bg \).*/\1$color/" "$zathura_config"
xmonad --restart
