#!/bin/sh

default_source=$(pamixer --list-sources | grep -e 'input.*Arctis' | awk '{print $2}' | sed 's/\"//g' )
pactl set-default-source $default_source
