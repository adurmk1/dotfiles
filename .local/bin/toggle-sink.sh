#!/bin/sh

sink=$(pactl get-default-sink)
speakers=$(pamixer --list-sinks | grep -e 'pci.*analog' | awk '{print $2}' | sed 's/\"//g' )
headphones=$(pamixer --list-sinks | grep -e 'usb.*stereo' | awk '{print $2}' | sed 's/\"//g' )
if [ $sink = $headphones ]; then
    pactl set-default-sink $speakers
    dunstify -i multimedia-volume-control -h string:x-dunst-stack-tag:sink 'Sound Output' 'Speakers'
    echo 'Sink: Speakers'
else
    pactl set-default-sink $headphones
    dunstify -i multimedia-volume-control -h string:x-dunst-stack-tag:sink 'Sound Output' 'Headphones'
    echo 'Sink: Headphones'
fi
