#!/bin/sh

ratbagctl 'Logitech G815 RGB MECHANICAL GAMING KEYBOARD' profile 0 led 0 set mode cycle duration 4500 brightness 255
ratbagctl 'Logitech G815 RGB MECHANICAL GAMING KEYBOARD' profile 0 led 1 set color 0000FF mode on
ratbagctl 'Logitech G815 RGB MECHANICAL GAMING KEYBOARD' profile 0 button 0 action set macro +KEY_LEFTMETA KEY_ENTER -KEY_LEFTMETA
ratbagctl 'Logitech G815 RGB MECHANICAL GAMING KEYBOARD' profile 0 button 1 action set macro +KEY_LEFTMETA KEY_E -KEY_LEFTMETA
ratbagctl 'Logitech G815 RGB MECHANICAL GAMING KEYBOARD' profile 0 button 2 action set macro +KEY_LEFTMETA KEY_C -KEY_LEFTMETA
ratbagctl 'Logitech G815 RGB MECHANICAL GAMING KEYBOARD' profile 0 button 3 action set macro +KEY_LEFTMETA KEY_TAB -KEY_LEFTMETA
ratbagctl 'Logitech G815 RGB MECHANICAL GAMING KEYBOARD' profile 0 button 4 action set macro +KEY_LEFTMETA KEY_F -KEY_LEFTMETA
ratbagctl 'Logitech G815 RGB MECHANICAL GAMING KEYBOARD' profile 1 led 0 set mode cycle duration 4500 brightness 255
ratbagctl 'Logitech G815 RGB MECHANICAL GAMING KEYBOARD' profile 1 led 1 set color 0000FF mode on
ratbagctl 'Logitech G815 RGB MECHANICAL GAMING KEYBOARD' profile 2 led 1 set mode cycle duration 2500 brightness 255
ratbagctl 'Logitech G815 RGB MECHANICAL GAMING KEYBOARD' profile 2 led 0 set mode cycle duration 2500 brightness 255
