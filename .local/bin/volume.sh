#!/bin/sh

if [ $1 = 'increase' ]; then
    pamixer --allow-boost -i 5
    volume=$(pamixer --get-volume)
    dunstify -i multimedia-volume-control -h string:x-dunst-stack-tag:volume Volume $volume% -h int:value:$volume
    echo Volume: $volume
elif [ $1 = 'decrease' ]; then
    pamixer --allow-boost -d 5
    volume=$(pamixer --get-volume)
    dunstify -i multimedia-volume-control -h string:x-dunst-stack-tag:volume Volume $volume% -h int:value:$volume
    echo Volume: $volume
elif [ $1 = 'toggle-mute' ]; then
    pamixer --toggle-mute
    mute=$(pamixer --get-mute)
    dunstify -i multimedia-volume-control -h string:x-dunst-stack-tag:mute Mute $mute
    echo Mute: $mute
elif [ $1 = 'toggle-mic-mute' ]; then
    source=$(pactl get-default-sink)
    pamixer --source $source --toggle-mute
    mic_mute=$(pamixer --source $source --get-mute)
    dunstify -i multimedia-volume-control -h string:x-dunst-stack-tag:mic-mute Mic mute $mic_mute
    echo Mic mute: $mic_mute
fi
