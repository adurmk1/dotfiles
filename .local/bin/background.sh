#!/bin/sh

new_background="$HOME/Pictures/Backgrounds/$1.png"
background="$HOME/Pictures/Backgrounds/background.png"
if [ -e "$new_background" ]; then
   ln -sfT "$new_background" "$background"
   feh --bg-fill "$background"
   echo "Background changed to $new_background."
else
   echo "The file $new_background doesn't exist!"
fi
