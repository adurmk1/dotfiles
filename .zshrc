# HISTORY SETTINGS
HISTFILE=$HOME/.zsh_history
HISTSIZE=10000
SAVEHIST=50000
setopt SHARE_HISTORY

# ENVIRONMENT VARIABLES
# Programs
export EDITOR='emacs -nw'
export VISUAL="emacsclient -c -a 'emacs'"
export PAGER='bat'
export BAT_PAGER='less'
export MANROFFOPT='-c'
export MANPAGER="sh -c 'col -bx | bat --paging=always -l man -p'"

# Starship
export STARSHIP_CONFIG="$HOME/.config/starship/starship.toml"

# ZSH PLUGINS
# Zsh config dir
zsh_config_dir="$HOME/.config/zsh"

# Extra completions
fpath=($zsh_config_dir/zsh-completions/src $fpath)

# Fzf tab completion
autoload -Uz compinit -d "$zsh_config_dir/dumps/zcompdump" && compinit -d "$zsh_config_dir/dumps/zcompdump"
source "$zsh_config_dir/fzf-tab/fzf-tab.plugin.zsh"
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
zstyle ':completion:*:git-checkout:*' sort false
zstyle ':completion:*' menu no
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'eza -1 --color=always $realpath'
eval "$(dircolors)"
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}

# Autosuggestions
source "$zsh_config_dir/zsh-autosuggestions/zsh-autosuggestions.zsh"

# Fast syntax highlighting
source "$zsh_config_dir/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh"

# Zsh history substring search
source "$zsh_config_dir/zsh-history-substring-search/zsh-history-substring-search.zsh"
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_FOUND=''
HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_NOT_FOUND=''
HISTORY_SUBSTRING_SEARCH_FUZZY=1
HISTORY_SUBSTRING_SEARCH_PREFIXED=1
HISTORY_SUBSTRING_SEARCH_ENSURE_UNIQUE=1

# Title updates
source "$zsh_config_dir/zsh-title-updates/zsh-title-updates.zsh"

# ZOXIDE
eval "$(zoxide init --cmd cd zsh)"

# STARSHIP
eval "$(starship init zsh)"

# STARTUP
fastfetch

# ALIASES
# Emacs
alias em='emacs -nw'
alias ec="emacsclient -nw -a 'emacs -nw'"
alias emacs-restart='systemctl --user restart emacs.service'

# Ls
alias ls='eza'
alias l='eza -lAh'
alias lh='eza -lh'

# Dirs
alias md='mkdir'
alias rd='rmdir'

# Grep
alias grep='grep --color=auto'

# Pacman
alias pac='sudo pacman'

# Objdump
alias odump='objdump -Matt --disassembler-color=on -d'

# Vallgrind
alias callgrind='valgrind --tool=callgrind'
alias cachegrind='valgrind --tool=cachegrind'

# Ttyper
alias ttypere='ttyper -l english1000'
alias ttyperc='ttyper -l c'
