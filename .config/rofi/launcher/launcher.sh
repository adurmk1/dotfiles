#!/bin/sh

# Directory
dir="$HOME/.config/rofi/launcher"

#Run
rofi \
    -show $1 \
    -theme "$dir/config.rasi"
