#!/bin/sh

# Directory
dir="$HOME/.config/rofi/powermenu"

# Infos
uptime=$(uptime -p | sed -e 's/up //g')
host=$(cat /proc/sys/kernel/hostname)

# Options
lock='🗝 Lock Screen'
logout='↪ Logout'
shutdown='⏻ Shutdown'
reboot='↺ Reboot'

# Rofi CMD
rofi_cmd() {
    rofi -dmenu \
        -p "$host" \
        -mesg "Uptime: $uptime" \
        -theme "$dir/config.rasi"
}

# Pass variables to rofi dmenu
run_rofi() {
    echo "$lock\n$logout\n$shutdown\n$reboot" | rofi_cmd
}

# Execute Command
run_cmd() {
    if [ $1 = 'shutdown' ]; then
        systemctl poweroff
    elif [ $1 = 'reboot' ]; then
        systemctl reboot
    elif [ $1 = 'logout' ]; then
        pkill xmonad
    elif [ $1 = 'lock' ]; then
        lock-screen.sh
    fi
}

# Actions
chosen=$(run_rofi)
case $chosen in
    $shutdown)
        run_cmd shutdown
        ;;
    $reboot)
        run_cmd reboot
        ;;
    $logout)
        run_cmd logout
        ;;
    $lock)
        run_cmd lock
        ;;
esac
