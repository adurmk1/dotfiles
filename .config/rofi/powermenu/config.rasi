// Powermenu config used by rofi

// Configuration
configuration {
    show-icons:        false;
}

// Global properties
@import                "~/.config/rofi/themes/theme.rasi"
@import                "~/.config/rofi/fonts.rasi"

// Main window
window {
    /* properties for window widget */
    transparency:        "real";
    location:          center;
    anchor:            center;
    border-color:      @selected;
    background-color:  transparent;
    text-color:        @foreground;
    children:          [ "textbox-prompt-colon", "prompt"];
}

textbox-prompt-colon {
    enabled:           true;
    expand:            false;
    str:               "⌬";
    padding:           8px 12px;
    border-radius:     0px;
    background-color:  @gear;
    text-color:        @background;
}
prompt {
    enabled:           true;
    padding:           8px;
    border-radius:     0px;
    background-color:  @active;
    text-color:        @background;
}

// Message
message {
    enabled:           true;
    margin:            0px;
    padding:           8px;
    border:            0px solid;
    border-radius:     0px;
    border-color:      @selected;
    background-color:  @background-alt;
    text-color:        @foreground;
}
textbox {
    background-color:  inherit;
    text-color:        inherit;
    vertical-align:    0.5;
    horizontal-align:  0.0;
    placeholder-color: @foreground;
    blink:             true;
    markup:            true;
}
error-message {
    padding:           8px;
    border:            0px solid;
    border-radius:     0px;
    border-color:      @selected;
    background-color:  @background;
    text-color:        @foreground;
}

// Listview
listview {
    enabled:           true;
    columns:           1;
    lines:             4;
    cycle:             true;
    dynamic:           true;
    scrollbar:         false;
    layout:            vertical;
    reverse:           false;
    fixed-height:      true;
    fixed-columns:     true;
    
    spacing:           5px;
    margin:            0px;
    padding:           0px;
    border:            0px solid;
    border-radius:     0px;
    border-color:      @selected;
    background-color:  transparent;
    text-color:        @foreground;
    cursor:            "default";
}

// Elements
element {
    enabled:           true;
    spacing:           0px;
    margin:            0px;
    padding:           8px;
    border:            0px solid;
    border-radius:     0px;
    border-color:      @selected;
    background-color:  transparent;
    text-color:        @foreground;
    cursor:            pointer;
}
element-text {
    background-color:  transparent;
    text-color:        inherit;
    cursor:            inherit;
    vertical-align:    0.5;
    horizontal-align:  0.0;
}
element selected.normal {
    background-color:  var(selected);
    text-color:        var(background);
}
