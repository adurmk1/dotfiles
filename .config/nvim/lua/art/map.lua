vim.g.mapleader = ' '
vim.g.localleader = ' '

vim.cmd('nmap <leader>w <C-w>')
vim.keymap.set('n', '<leader>fe', vim.cmd.Ex)

-- Double Quotes, etc.
vim.api.nvim_exec([[inoremap ( ()<Left>]], true)
vim.api.nvim_exec([[inoremap [ []<Left>]], true)
vim.api.nvim_exec([[inoremap { {}<Left>]], true)
vim.api.nvim_exec([[inoremap " ""<Left>]], true)
vim.api.nvim_exec([[inoremap ' ''<Left>]], true)

-- Telescope
local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', builtin.find_files, { desc = 'Telescope find files' })
vim.keymap.set('n', '<leader>fg', builtin.live_grep, { desc = 'Telescope live grep' })
vim.keymap.set('n', '<leader>fb', builtin.buffers, { desc = 'Telescope buffers' })
vim.keymap.set('n', '<leader>fh', builtin.help_tags, { desc = 'Telescope help tags' })

-- Telescope File Manager
require("telescope").load_extension "file_browser"
vim.api.nvim_set_keymap("n", "<leader>.", ":Telescope file_browser<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<leader>fb", ":Telescope file_browser<CR>", { noremap = true })
