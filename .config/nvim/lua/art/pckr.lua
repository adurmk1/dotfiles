local function bootstrap_pckr()
  local pckr_path = vim.fn.stdpath('data') .. '/pckr/pckr.nvim'

  if not (vim.uv or vim.loop).fs_stat(pckr_path) then
    vim.fn.system({
      'git',
      'clone',
      '--filter=blob:none',
      'https://github.com/lewis6991/pckr.nvim',
      pckr_path
    })
  end

  vim.opt.rtp:prepend(pckr_path)
end

bootstrap_pckr()

local cmd = require('pckr.loader.cmd')
local keys = require('pckr.loader.keys')

require('pckr').add{
  -- Doom One Theme
  { 'NTBBloodbath/doom-one.nvim',
    setup = function()
      -- Add color to cursor
      vim.g.doom_one_cursor_coloring = false
      -- Set :terminal colors
      vim.g.doom_one_terminal_colors = true
      -- Enable italic comments
      vim.g.doom_one_italic_comments = false
      -- Enable TS support
      vim.g.doom_one_enable_treesitter = false
      -- Color whole diagnostic text or only underline
      vim.g.doom_one_diagnostics_text_color = false
      -- Enable transparent background
      vim.g.doom_one_transparent_background = false
      
      -- Pumblend transparency
      vim.g.doom_one_pumblend_enable = false
      vim.g.doom_one_pumblend_transparency = 20
      
      -- Plugins integration
      vim.g.doom_one_plugin_neorg = false
      vim.g.doom_one_plugin_barbar = false
      vim.g.doom_one_plugin_telescope = false
      vim.g.doom_one_plugin_neogit = false
      vim.g.doom_one_plugin_nvim_tree = false
      vim.g.doom_one_plugin_dashboard = false
      vim.g.doom_one_plugin_startify = false
      vim.g.doom_one_plugin_whichkey = true
      vim.g.doom_one_plugin_indent_blankline = true
      vim.g.doom_one_plugin_vim_illuminate = false
      vim.g.doom_one_plugin_lspsaga = false
    end,
    config = function()
      vim.cmd('colorscheme doom-one') 
    end
  };

  -- Telescope
  { 'nvim-telescope/telescope.nvim',
    requires = 'nvim-lua/plenary.nvim',
  };

  -- Telescope File Browser
  { 'nvim-telescope/telescope-file-browser.nvim',
    requires = {'nvim-lua/plenary.nvim', 'nvim-telescope/telescope.nvim'},
  };

  -- Lualine
  { 'nvim-lualine/lualine.nvim',
    requires = 'nvim-tree/nvim-web-devicons',
    config = function()
      require('lualine').setup{
        theme = 'auto',
      }
    end
  };

  -- Whichkey
  { 'folke/which-key.nvim',
    config = function()
      vim.o.timeout = true
      vim.o.timeoutlen = 300
      require('which-key').setup{}
    end
  };

  -- Indent Blankline
  { 'lukas-reineke/indent-blankline.nvim',
    config = function()
      local hooks = require('ibl.hooks')
      local highlight = {'Blankline'}
      hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
          vim.api.nvim_set_hl(0, "Blankline", {fg = '#21242b'})
        end)
      require('ibl').setup {
        indent = {highlight = highlight},
        scope = {enabled = false},
      }
    end
  };

  -- Comment
  { 'terrortylor/nvim-comment',
    config = function ()
      require('nvim_comment').setup({
        comment_empty = false,
        line_mapping = 'gcc',
        operator_mapping = 'gc',
      })
    end
  };
}
