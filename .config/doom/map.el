;;; $DOOMDIR/map.el -*- lexical-binding: t; -*-

;; Capitalize words
(map! :n "gC" #'capitalize-dwim)

(map! :leader
      :desc "Calendar" "oc" #'cfw:open-org-calendar
      :desc "Calc"     "oC" #'calc)

(map! :leader
      :desc "Ripgreg"          "fg" #'consult-ripgrep
      :desc "Ripgrep --hidden" "fG" #'art/consult-ripgrep-hidden)

;; Harpoon
(map! :leader
      :desc "Harpoon go to 1" "1" #'harpoon-go-to-1
      :desc "Harpoon go to 2" "2" #'harpoon-go-to-2
      :desc "Harpoon go to 3" "3" #'harpoon-go-to-3
      :desc "Harpoon go to 4" "4" #'harpoon-go-to-4
      :desc "Harpoon go to 5" "5" #'harpoon-go-to-5
      :desc "Harpoon go to 6" "6" #'harpoon-go-to-6
      :desc "Harpoon go to 7" "7" #'harpoon-go-to-7
      :desc "Harpoon go to 8" "8" #'harpoon-go-to-8
      :desc "Harpoon go to 9" "9" #'harpoon-go-to-9
      (:prefix ("fh" . "harpoon")
       :desc "Add file"    "a" #'harpoon-add-file
       :desc "Toggle file" "t" #'harpoon-toggle-file
       :desc "Clear"       "c" #'harpoon-clear
       :desc "Hydra"       "h" #'harpoon-quick-menu-hydra
       :desc "Menu"        "m" #'harpoon-toggle-quick-menu
       :desc "Go to 1"     "1" #'harpoon-go-to-1
       :desc "Go to 2"     "2" #'harpoon-go-to-2
       :desc "Go to 3"     "3" #'harpoon-go-to-3
       :desc "Go to 4"     "4" #'harpoon-go-to-4
       :desc "Go to 5"     "5" #'harpoon-go-to-5
       :desc "Go to 6"     "6" #'harpoon-go-to-6
       :desc "Go to 7"     "7" #'harpoon-go-to-7
       :desc "Go to 8"     "8" #'harpoon-go-to-8
       :desc "Go to 9"     "9" #'harpoon-go-to-9)
      (:prefix ("fhd" . "delete")
       :desc "Delete 1" "1" #'harpoon-delete-1
       :desc "Delete 2" "2" #'harpoon-delete-2
       :desc "Delete 3" "3" #'harpoon-delete-3
       :desc "Delete 4" "4" #'harpoon-delete-4
       :desc "Delete 5" "5" #'harpoon-delete-5
       :desc "Delete 6" "6" #'harpoon-delete-6
       :desc "Delete 7" "7" #'harpoon-delete-7
       :desc "Delete 8" "8" #'harpoon-delete-8
       :desc "Delete 9" "9" #'harpoon-delete-9))

;; Org
(map! :map evil-org-mode-map :localleader
      "E"  #'org-babel-execute-src-block)

(map! :map evil-org-mode-map :leader
      :desc "Insert LaTeX class options" "il" #'org-latex-insert-latex-class-options
      :desc "Insert LateX header"        "iL" #'org-latex-insert-latex-header)

;; Zig
(map! :map zig-mode-map
      :leader
      (:prefix ("z" . "zig")
       :desc "Build"         "b" #'zig-compile
       :desc "Build run"     "r" #'art/zig-build-run
       :desc "Build test"    "t" #'art/zig-build-test
       :desc "Run buffer"    "R" #'zig-run
       :desc "Test buffer"   "T" #'zig-test-buffer
       :desc "Format buffer" "f" #'zig-format-buffer))

;; DAP
(map! :map dap-mode-map
      :leader
      (:prefix ("d" . "dap")
       :desc "Next"              "n" #'dap-next
       :desc "Step in"           "i" #'dap-step-in
       :desc "Step out"          "o" #'dap-step-out
       :desc "Continue"          "c" #'dap-continue
       :desc "Hydra"             "h" #'dap-hydra
       :desc "Disconnect"        "D" #'dap-disconnect)
      (:prefix ("dd" ."debug")
       :desc "Debug"             "d" #'dap-debug
       :desc "Recent"            "r" #'dap-debug-recent
       :desc "Restart"           "R" #'dap-debug-restart
       :desc "Last"              "l" #'dap-debug-last)
      (:prefix ("db" . "breakpoint")
       :desc "Toggle"            "t" #'dap-breakpoint-toggle
       :desc "Add"               "a" #'dap-breakpoint-add
       :desc "Delete"            "d" #'dap-breakpoint-delete
       :desc "Delete all"        "D" #'dap-breakpoint-delete-all
       :desc "Condition"         "c" #'dap-breakpoint-condition
       :desc "Hit count"         "h" #'dap-breakpoint-hit-condition
       :desc "Log message"       "l" #'dap-breakpoint-log-message)
      (:prefix ("de" . "eval")
       :desc "Eval"              "e" #'dap-eval
       :desc "Region"            "r" #'dap-eval-region
       :desc "Thing at point"    "p" #'dap-eval-thing-at-point
       :desc "Add expression"    "a" #'dap-ui-expressions-add
       :desc "Remove expression" "R" #'dap-ui-expressions-remove
       :desc "Repl"              "l" #'dap-ui-repl)
      (:prefix ("ds" . "session")
       :desc "delete"            "d" #'dap-delete-session
       :desc "delete all"        "D" #'dap-delete-all-sessions))

;; Playerctl
(map! :leader
      (:prefix ("#" . "playerctl")
       :desc "Play/Pause"     "SPC" #'art/playerctl-play-pause
       :desc "Next"           "n"   #'art/playerctl-next
       :desc "Prev"           "p"   #'art/playerctl-prev
       :desc "Toggle looping" "l"   #'art/playerctl-toggle-loop))

(define-key! [remap man] #'man)

(map! :leader
      (:prefix ("-" . "art")
       :desc "External terminal"          "t" #'art/open-external-terminal
       :desc "Man"                        "m" #'man
       :desc "Consult man"                "M" #'consult-man
       :desc "Scratch buffer"             "s" #'scratch-buffer
       :desc "Screenshot"                 "S" #'screenshot
       :desc "Disassembly"                "d" #'disaster
       :desc "Restart rainbow delimiters" "r" #'art/rainbow-delimiter-restart
       :desc "Emacs state"                "e" #'evil-emacs-state)
      (:prefix ("-f" . "file")
       :desc "Complete"         "c" #'comint-dynamic-complete-filename
       :desc "Fd"               "f" #'consult-fd
       :desc "Ripgrep"          "r" #'consult-ripgrep
       :desc "Ripgrep --hidden" "R" #'art/consult-ripgrep-hidden))
