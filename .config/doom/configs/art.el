;;; $DOOMDIR/configs/art.el -*- lexical-binding: t; -*-

(defgroup art nil
  "My own Emacs features."
  :group 'emacs)

(defcustom art/external-terminal "tabbed -c alacritty --embed"
  "External terminal used by art/open-external-terminal by default."
  :group 'art
  :type 'string)

(defun art/open-external-terminal (&optional TERMINAL)
  "Execute external terminal synchronously in seperate process.

By default art/external-terminal is executed.
If TERMINAL is provided TERMINAL is executed."
  (interactive)
  (let ((terminal (or TERMINAL art/external-terminal)))
  (async-start (lambda ()
                 (call-process-shell-command terminal)))))

(require 'consult)
(defun art/consult-ripgrep-hidden (&optional DIR INITIAL)
  "Thin wrapper around `consult-ripgrep' with the --hidden flag."
  (interactive)
  (let ((consult-ripgrep-args (concat consult-ripgrep-args " --hidden")))
    (consult-ripgrep DIR INITIAL)))

(defun art/rainbow-delimiter-restart ()
  "Restart rainbow-delimiters mode in current buffer."
  (interactive)
  (rainbow-delimiters-mode-disable)
  (rainbow-delimiters-mode-enable))

(defun art/zig-build-run ()
  "Run `zig build run' in current directory."
  (interactive)
  (shell-command "zig build run"))

(defun art/zig-build-test ()
  "Run `zig build test' in current directory."
  (interactive)
  (shell-command "zig build test"))

(defun art/playerctl-play-pause ()
  "Use `playerctl' to play/pause playback."
  (interactive)
  (let ((status (shell-command-to-string "playerctl status")))
    (shell-command "playerctl play-pause")
    (message (if (string-equal status "Playing\n")
                 "Paused playback"
               "Resumed playback"))))

(defun art/playerctl-next ()
  "Use `playerctl' to skip to next track."
  (interactive)
  (shell-command "playerctl next")
  (message "Skipped to next track"))

(defun art/playerctl-prev ()
  "Use `playerctl' to skip to previous track."
  (interactive)
  (shell-command "playerctl previous")
  (message "Skipped to previous track"))

(defun art/playerctl-toggle-loop ()
  "Use `playerctl' to toggle track looping."
  (interactive)
  (let ((loop (shell-command-to-string "playerctl loop")))
    (if (string-equal loop "Playlist\n")
        (progn (shell-command "playerctl loop Track")
               (message "Looping over track"))
      (progn (shell-command "playerctl loop Playlist")
             (message "Looping over playlist")))))
