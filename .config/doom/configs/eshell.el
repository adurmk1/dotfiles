;;; $DOOMDIR/configs/eshell.el -*- lexical-binding: t; -*-

(after! eshell
  (set-eshell-alias!
   "ls" "eza"
   "l"  "eza -lAh"
   "lh" "eza -lh"
   "md" "mkdir"
   "rd" "rmdir"
   "pac" "sudo pacman"
   "objdump" "objdump -Matt --disassembler-color=on -d"
   "callgrind" "vallgrind --tool=callgrind"
   "cachegrind" "vallgrind --tool=cachegrind"))
