;;; $DOOMDIR/configs/lsp.el -*- lexical-binding: t; -*-

;; Disable lsp warning if there is no matched client
(setq lsp-warn-no-matched-clients nil
      lsp-enable-suggest-server-download nil)

;; Breadcrumbs in Modeline
(setq lsp-headerline-breadcrumb-segments '(symbols))
(after! lsp-mode
  (require 'lsp-headerline)
  (setq mode-line-misc-info
        (cons (list '(:eval (if (lsp-mode)
                                (replace-regexp-in-string "^> " "" (concat (lsp-headerline--build-string) "  "))
                              "" ))) mode-line-misc-info))

;; CCLS
(after! ccls
  (set-lsp-priority! 'ccls 1))

;; LSP headerline breadcrumb faces
  (custom-set-faces!
    `(lsp-headerline-breadcrumb-path-info-face :underline (:style wave :color ,(doom-color 'green)) :inherit lsp-headerline-breadcrumb-path-face)
    `(lsp-headerline-breadcrumb-path-hint-face :underline (:style wave :color ,(doom-color 'green)) :inherit lsp-headerline-breadcrumb-path-face)
    `(lsp-headerline-breadcrumb-path-warning-face :underline (:style wave :color ,(doom-color 'yellow)) :inherit lsp-headerline-breadcrumb-path-face)
    `(lsp-headerline-breadcrumb-path-error-face :underline (:style wave :color ,(doom-color 'red)) :inherit lsp-headerline-breadcrumb-path-face)
    `(lsp-headerline-breadcrumb-symbols-info-face :underline (:style wave :color ,(doom-color 'green)) :inherit lsp-headerline-breadcrumb-path-face)
    `(lsp-headerline-breadcrumb-symbols-hint-face :underline (:style wave :color ,(doom-color 'green)) :inherit lsp-headerline-breadcrumb-path-face)
    `(lsp-headerline-breadcrumb-symbols-warning-face :underline (:style wave :color ,(doom-color 'yellow)) :inherit lsp-headerline-breadcrumb-path-face)
    `(lsp-headerline-breadcrumb-symbols-error-face :underline (:style wave :color ,(doom-color 'red)) :inherit lsp-headerline-breadcrumb-path-face)))
