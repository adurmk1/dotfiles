;;; $DOOMDIR/configs/tree-sitter.el -*- lexical-binding: t; -*-

;; Define new tree sitter hl faces
(defface tree-sitter-hl-face:<example>
  '((default :inherit font-lock-function-call-face))
  "Face for constants in Zig."
  :group 'tree-sitter-hl-faces)

(after! tree-sitter
  (require 'tree-sitter-hl)

  (add-function :before-until tree-sitter-hl-face-mapping-function
                (lambda (capture-name)
                  (pcase capture-name
                    ("<name>"   'tree-sitter-hl-face:<example>))))

  (tree-sitter-hl-add-patterns 'c
    [])

  ;; Set tree-sitter-hl faces
  (custom-set-faces!
    `(tree-sitter-hl-face:attribute :weight medium :foreground ,(doom-color 'blue))
    `(tree-sitter-hl-face:boolean :weight medium :foreground ,(doom-color 'violet))
    `(tree-sitter-hl-face:comment :weight medium :foreground ,(doom-color 'base5))
    `(tree-sitter-hl-face:conditional :weight medium :foreground ,(doom-color 'blue))
    `(tree-sitter-hl-face:conditional.ternary :weight medium :foreground ,(doom-color 'blue))
    `(tree-sitter-hl-face:constant :weight semi-bold :foreground ,(doom-color 'violet))
    `(tree-sitter-hl-face:constant.builtin :weight semi-bold :foreground ,(doom-color 'violet))
    `(tree-sitter-hl-face:constructor :weight semi-bold :foreground ,(doom-color 'yellow))
    `(tree-sitter-hl-face:doc :weight medium :foreground ,(doom-lighten (doom-color 'base5) 0.25))
    `(tree-sitter-hl-face:embedded :weight medium :foreground ,(doom-color 'fg))
    `(tree-sitter-hl-face:escape :weight medium :foreground ,(doom-color 'blue))
    `(tree-sitter-hl-face:exception :weight medium :foreground ,(doom-color 'blue))
    `(tree-sitter-hl-face:function :weight medium :foreground ,(doom-color 'magenta))
    `(tree-sitter-hl-face:function.builtin :weight medium :foreground ,(doom-color 'magenta))
    `(tree-sitter-hl-face:function.call :weight medium :foreground ,(doom-color 'blue))
    `(tree-sitter-hl-face:function.macro :weight bold :foreground ,(doom-color 'magenta))
    `(tree-sitter-hl-face:function.method :weight medium :slant italic :foreground ,(doom-color 'magenta))
    `(tree-sitter-hl-face:function.method.call :weight medium :slant italic :foreground ,(doom-color 'blue))
    `(tree-sitter-hl-face:function.special :weight medium :foreground ,(doom-color 'magenta))
    `(tree-sitter-hl-face:keyword :weight medium :foreground ,(doom-color 'blue))
    `(tree-sitter-hl-face:keyword.conditional :weight medium :foreground ,(doom-color 'blue))
    `(tree-sitter-hl-face:keyword.directive :weight medium :foreground ,(doom-color 'blue))
    `(tree-sitter-hl-face:keyword.repeat :weight medium :foreground ,(doom-color 'blue))
    `(tree-sitter-hl-face:label :weight bold :foreground ,(doom-color 'magenta))
    `(tree-sitter-hl-face:method :weight medium :slant italic :foreground ,(doom-color 'magenta))
    `(tree-sitter-hl-face:method.call :weight medium :slant italic :foreground ,(doom-color 'blue))
    `(tree-sitter-hl-face:noise :weight medium :foreground ,(doom-color 'fg))
    `(tree-sitter-hl-face:number :weight semi-bold :foreground ,(doom-color 'orange))
    `(tree-sitter-hl-face:operator :weight medium :foreground ,(doom-color 'blue))
    `(tree-sitter-hl-face:property :weight medium :slant italic :foreground ,(doom-color 'violet))
    `(tree-sitter-hl-face:property.definition :weight medium :slant italic :foreground ,(doom-lighten (doom-color 'magenta) 0.4))
    `(tree-sitter-hl-face:punctuation :weight medium :foreground ,(doom-color 'fg))
    `(tree-sitter-hl-face:punctuation.bracket :weight medium :foreground ,(doom-color 'fg))
    `(tree-sitter-hl-face:punctuation.delimiter :weight medium :foreground ,(doom-color 'fg))
    `(tree-sitter-hl-face:punctuation.special :weight medium :foreground ,(doom-color 'fg))
    `(tree-sitter-hl-face:repeat :weight medium :foreground ,(doom-color 'blue))
    `(tree-sitter-hl-face:string :weight medium :foreground ,(doom-color 'green))
    `(tree-sitter-hl-face:string.special :weight medium :foreground ,(doom-color 'green))
    `(tree-sitter-hl-face:tag :weight medium :foreground ,(doom-color 'magenta))
    `(tree-sitter-hl-face:type :weight medium :foreground ,(doom-color 'yellow))
    `(tree-sitter-hl-face:type.argument :weight medium :foreground ,(doom-color 'yellow))
    `(tree-sitter-hl-face:type.builtin :weight medium :foreground ,(doom-color 'yellow))
    `(tree-sitter-hl-face:type.parameter :weight medium :foreground ,(doom-color 'yellow))
    `(tree-sitter-hl-face:variable :weight medium :foreground ,(doom-lighten (doom-color 'magenta) 0.4))
    `(tree-sitter-hl-face:variable.builtin :weight medium :foreground ,(doom-lighten (doom-color 'magenta) 0.4))
    `(tree-sitter-hl-face:variable.parameter :weight medium :foreground ,(doom-lighten (doom-color 'magenta) 0.4))
    `(tree-sitter-hl-face:variable.special :weight medium :foreground ,(doom-lighten (doom-color 'magenta) 0.4))

    ;; tree-sitter-hl-faces defined by me
    `(tree-sitter-hl-face:<example> :weight medium :foreground ,(doom-color 'blue))))
