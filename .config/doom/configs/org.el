;;; $DOOMDIR/configs/org.el -*- lexical-binding: t; -*-

;; Org settings
(setq org-directory "~/Documents/Org/"
      org-startup-with-inline-images t)

;; Suppress warnings
(add-to-list 'warning-suppress-types '(org-element-cache))

;; Org mode packages
(add-hook! org-mode #'org-auto-tangle-mode #'org-fragtog-mode)

;; Auto fill
(add-hook! org-mode #'auto-fill-mode (setq fill-column 120))

;; Org superstar settings
(setq org-superstar-headline-bullets-list '("◉" "○" "◉" "○" "◉" "○"))

;; Org agenda
(setq org-deadline-warning-days 7
      org-agenda-files (list "~/Documents/Org/Agenda"))

;; Org latex settings
(setq org-latex-image-default-scale "0.7"
      org-latex-images-centered nil)

;; Org engrave faces
(setq org-latex-src-block-backend 'engraved
      org-latex-engraved-theme t)

;; Org calender
(setq cfw:org-agenda-schedule-args '(:timestamp :scheduled))

;; Org roam
(setq org-roam-directory "~/Documents/Roam"
      org-roam-capture-templates '(("d" "default" plain
                                    "%?"
                                    :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
                                    :unnarrowed t)
                                   ("n" "note" plain
                                    "\n* Infos\n- Title: ${title}\n- Subject: %^{Subject}\n\n* Note\n%?"
                                    :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
                                    :unnarrowed t)
                                   ("p" "project" plain
                                    "* Goals\n\n%?\n\n* Tasks\n\n"
                                    :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
                                    :unnarrowed t)))

(after! org
  ;; Org tempo
  (require 'org-tempo)
  (add-to-list 'org-structure-template-alist '("cc"  . "src C :results drawer"))
  (add-to-list 'org-structure-template-alist '("el"  . "src emacs-lisp :results drawer"))
  (add-to-list 'org-structure-template-alist '("sh"  . "src shell"))
  (add-to-list 'org-structure-template-alist '("z"   . "src zig"))
  (add-to-list 'org-structure-template-alist '("zig" . "src zig"))

  ;; Org babel
  (add-to-list 'org-babel-load-languages '(c . t)))

;; My own functionality
(defcustom org-latex-default-latex-header ""
  "LaTeX header used by org-insert-latex-options by default"
  :group 'org-latex
  :type 'string)

(defcustom org-latex-default-latex-class-options "[a4paper]"
  "LaTeX header used by org-insert-latex-options by default"
  :group 'org-latex
  :type 'string)

(defun org-latex-insert-latex-header (&optional HEADER)
  "Inserts LaTeX header into current org-mode file.

By default org-latex-default-latex-header is inserted.
If HEADER is provided HEADER is inserted."
  (interactive)
  (if (eq major-mode 'org-mode)
      (save-excursion
        (widen)
        (goto-char 1)
        (let ((latex-header
               (concat "#+latex_header: " (or HEADER org-latex-default-latex-header))))
          (if (search-forward "#+author" (point-max) t)
              (progn (end-of-line)
                     (insert (concat "\n" latex-header)))
            (if (search-forward "#+title" (point-max) t)
                (progn (end-of-line)
                       (insert (concat "\n" latex-header)))
              (insert (concat latex-header "\n"))))))
    (message "Major mode is not org-mode")))

(defun org-latex-insert-latex-class-options (&optional OPTIONS)
  "Inserts LaTeX class options into current org-mode file.

By default org-latex-default-latex-class-options is inserted.
If OPTIONS is provided OPTIONS is inserted."
  (interactive)
  (if (eq major-mode 'org-mode)
      (save-excursion
        (widen)
        (goto-char 1)
        (let ((latex-class-options
               (concat "#+latex_class_options: " (or OPTIONS org-latex-default-latex-class-options))))
          (if (search-forward "#+author" (point-max) t)
              (progn (end-of-line)
                     (insert (concat "\n" latex-class-options)))
            (if (search-forward "#+title" (point-max) t)
                (progn (end-of-line)
                       (insert (concat "\n" latex-class-options)))
              (insert (concat latex-class-options "\n"))))))
    (message "Major mode is not org-mode")))
