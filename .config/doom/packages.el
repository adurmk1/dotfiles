;; -*- no-byte-compile: t; -*-
;;; $DOOMDIR/packages.el

;; Disable packages
(package! doom-snippets :ignore t)
(package! cuda-mode :ignore t)
(package! cmake-mode :ignore t)

;; Other packages
(package! elcord)
(package! engrave-faces)
(package! harpoon)
(package! org-auto-tangle)
(package! org-fragtog)
(package! rainbow-mode)
(package! screenshot :recipe (:host github :repo "tecosaur/screenshot"))
