;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; User
(setq user-full-name "<name>"
      user-mail-address "<mail>")

;; Font
(setq doom-font (font-spec :family "Source Code Pro" :size 14 :weight 'medium)
      doom-variable-pitch-font (font-spec :family "SourceCodeVF" :size 14 :weight 'medium))

;; Theme
(setq doom-theme 'doom-one)

;; Opacity
(add-to-list 'default-frame-alist '(alpha-background . 90))

;; Line numbers
(setq display-line-numbers-type 'relative)

;; Scroll margin
(setq scroll-margin 7)

;; Fill column
(setq-default fill-column 80)
(add-hook 'prog-mode-hook #'display-fill-column-indicator-mode)

;; Scratch Buffer
(setq initial-major-mode 'org-mode
      initial-scratch-message "#+title: Scratch\n")

;; Make evil not use the system clipboard by default
(setq select-enable-clipboard nil)

;; Projectile
(setq projectile-project-search-path "~/Projects")

;; LSP
(load! "configs/lsp")

;; DAP
(setq dap-auto-configure-features '(sessions locals breakpoints expressions controls tooltips))

;; Tree-sitter
(load! "configs/tree-sitter")

;; Eshell
(load! "configs/eshell")

;; Org
(load! "configs/org")

;; Snippets
(set-file-templates! '("/main\\.zig\\'" :trigger "main" :mode zig-mode))

;; Harpoon
(setq harpoon-separate-by-branch t
      harpoon-project-package 'projectile
      harpoon-without-project-function 'harpoon--current-file-directory)

;; Rainbow mode
(add-hook! (prog-mode conf-mode) #'rainbow-mode)

;; Treemacs
(setq treemacs-is-never-other-window nil)

;; PDF Tools
(pdf-loader-install t t nil nil)

;; Vterm
(setq vterm-always-compile-module t
      vterm-kill-buffer-on-exit t)

;; Screenshot
(setq screenshot-font-family "Source Code Pro"
      screenshot-font-size 14
      screenshot-shadow-intensity 0)

;; My own functionality
(load! "configs/art")

;; Keybinds
(load! "map")
