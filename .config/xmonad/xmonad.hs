-- IMPORTS
-- Base
import XMonad
import qualified XMonad.StackSet as W

-- Data
import Data.Monoid
import qualified Data.Map as M

-- Utils
import XMonad.Util.EZConfig (mkKeymap)
import XMonad.Util.NamedScratchpad
import XMonad.Util.SpawnOnce (spawnOnce)
import XMonad.Util.WorkspaceCompare (filterOutWs)

-- Layouts
import XMonad.Layout.Grid
import XMonad.Layout.NoBorders (noBorders)
import XMonad.Layout.Spacing (spacingWithEdge)
import XMonad.Layout.Spiral
import XMonad.Layout.ToggleLayouts

-- Hooks
import XMonad.Hooks.EwmhDesktops (ewmh, addEwmhWorkspaceSort)
import XMonad.Hooks.ManageDocks (avoidStruts, docks)
import XMonad.Hooks.ManageHelpers (isDialog)

-- Actions
import XMonad.Actions.CycleWS (nextWS, prevWS, shiftToNext, shiftToPrev)
import XMonad.Actions.OnScreen (greedyViewOnScreen)
import XMonad.Actions.PhysicalScreens
  (
    onPrevNeighbour,
    onNextNeighbour,
    horizontalScreenOrderer,
  )

----------------------------------------------------------------------------------------------------
-- APPLICATIONS
myTerminal :: String
myTerminal = "tabbed -c alacritty --embed"

myEditor :: String
myEditor = "emacsclient -c -a 'emacs'"

myBrowser :: String
myBrowser = "librewolf"

myMenu :: String
myMenu = "launcher.sh drun"

myRun :: String
myRun = "launcher.sh run"

myFileManager :: String
myFileManager = "thunar"

myScreenshooter :: String
myScreenshooter = "xfce4-screenshooter"

myLock :: String
myLock = "lock-screen.sh"

----------------------------------------------------------------------------------------------------
-- WORKSPACES
myWorkspaces :: [WorkspaceId]
myWorkspaces = ["1", "2", "3", "4", "5", "6", "7", "8"]

----------------------------------------------------------------------------------------------------
-- BORDERS
myNormalBorderColor :: String
myNormalBorderColor = "#282c34"

myFocusedBorderColor :: String
myFocusedBorderColor = "#51afef"

myBorderWidth :: Dimension
myBorderWidth = 1

----------------------------------------------------------------------------------------------------
-- FOCUS
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = False

myClickJustFocuses :: Bool
myClickJustFocuses = False

----------------------------------------------------------------------------------------------------
-- NAMED SCRATCHPADS
myScratchpads :: [NamedScratchpad]
myScratchpads =
  [ NS "Emacs"              emacs        findEmacs        manageFloat
  , NS "Terminal"           term         findTerm         manageFloat
  , NS "KeePassXC"          keePassXC    findKeePassXC    manageFloat
  , NS "Spotify"            spotify      findSpotify      manageFloat
  , NS "Discord"            discord      findDiscord      manageFloat
  ]
    where
      emacs              = "emacs -T emacsScratchpad"
      findEmacs          = title =? "emacsScratchpad"
      term               = "tabbed -c -ntermScratchpad alacritty --embed"
      findTerm           = appName =? "termScratchpad"
      keePassXC          = "keepassxc"
      findKeePassXC      = className =? "KeePassXC"
      spotify            = "spotify"
      findSpotify        = className =? "Spotify"
      discord            = "discord"
      findDiscord        = className =? "discord"
      manageFloat        = customFloating $ W.RationalRect l t w h
        where
          l = 0.04
          t = 0.04
          w = 0.92
          h = 0.92

----------------------------------------------------------------------------------------------------
-- KEYBINDINGS
-- Mod Key
myModMask :: KeyMask
myModMask = mod4Mask

myKeys :: XConfig Layout -> M.Map (KeyMask, KeySym) (X ())
myKeys = \c -> mkKeymap c $
----------------------------------------------------------------------------------------------------
-- Launch programs
  [ ("M-<Return>",             spawn myTerminal)
  , ("M-t",                    spawn myTerminal)
  , ("M-<Space>",              spawn myMenu)
  , ("M-S-<Space>",            spawn myRun)
  , ("M-e",                    spawn myEditor)
  , ("<XF86Favorites>",        spawn myEditor)
  , ("M-r",                    spawn myFileManager)
  , ("M-b",                    spawn myBrowser)
  , ("<Print>",                spawn myScreenshooter)
  , ("M-C-S-l",                spawn myLock)
  , ("<XF86Display>",          spawn "arandr")

----------------------------------------------------------------------------------------------------
-- Session Controls
  , ("M-C-q",                  spawn "powermenu.sh")
  , ("M-C-S-q",                spawn "xmonad --recompile; xmonad --restart")

----------------------------------------------------------------------------------------------------
-- Window Manager Controls
  , ("M-c",                    kill)
  , ("M-<Tab>",                sendMessage NextLayout)
  , ("M-S-<Tab>",              sendMessage $ JumpToLayout "Tall")
  , ("M-f",                    sendMessage ToggleLayout)
  , ("M-j",                    windows W.focusDown)
  , ("M-k",                    windows W.focusUp)
  , ("M-m",                    windows W.focusMaster)
  , ("M-S-j",                  windows W.swapDown)
  , ("M-S-k",                  windows W.swapUp)
  , ("M-S-m",                  windows W.swapMaster)
  , ("M-C-h",                  sendMessage Shrink)
  , ("M-C-l",                  sendMessage Expand)
  , ("M-,",                    sendMessage $ IncMasterN 1)
  , ("M-.",                    sendMessage $ IncMasterN (-1))
  , ("M-C-S-t",                withFocused $ windows . W.sink)
  , ("M-1",                    windows $ W.greedyView $ myWorkspaces !! 0)
  , ("M-2",                    windows $ W.greedyView $ myWorkspaces !! 1)
  , ("M-3",                    windows $ W.greedyView $ myWorkspaces !! 2)
  , ("M-4",                    windows $ W.greedyView $ myWorkspaces !! 3)
  , ("M-5",                    windows $ W.greedyView $ myWorkspaces !! 4)
  , ("M-6",                    windows $ W.greedyView $ myWorkspaces !! 5)
  , ("M-7",                    windows $ W.greedyView $ myWorkspaces !! 6)
  , ("M-8",                    windows $ W.greedyView $ myWorkspaces !! 7)
  , ("M-S-1",                  windows $ W.shift $ myWorkspaces !! 0)
  , ("M-S-2",                  windows $ W.shift $ myWorkspaces !! 1)
  , ("M-S-3",                  windows $ W.shift $ myWorkspaces !! 2)
  , ("M-S-4",                  windows $ W.shift $ myWorkspaces !! 3)
  , ("M-S-5",                  windows $ W.shift $ myWorkspaces !! 4)
  , ("M-S-6",                  windows $ W.shift $ myWorkspaces !! 5)
  , ("M-S-7",                  windows $ W.shift $ myWorkspaces !! 6)
  , ("M-S-8",                  windows $ W.shift $ myWorkspaces !! 7)
  , ("M-l",                    onNextNeighbour horizontalScreenOrderer W.view)
  , ("M-<Right>",              onNextNeighbour horizontalScreenOrderer W.view)
  , ("M-h",                    onPrevNeighbour horizontalScreenOrderer W.view)
  , ("M-<Left>",               onPrevNeighbour horizontalScreenOrderer W.view)
  , ("M-S-l",                  onNextNeighbour horizontalScreenOrderer W.shift >> onNextNeighbour horizontalScreenOrderer W.view)
  , ("M-S-<Right>",            onNextNeighbour horizontalScreenOrderer W.shift >> onNextNeighbour horizontalScreenOrderer W.view)
  , ("M-S-h",                  onPrevNeighbour horizontalScreenOrderer W.shift >> onPrevNeighbour horizontalScreenOrderer W.view)
  , ("M-S-<Left>",             onPrevNeighbour horizontalScreenOrderer W.shift >> onPrevNeighbour horizontalScreenOrderer W.view)
  , ("M-<Up>",                 nextWS)
  , ("M-<Down>",               prevWS)
  , ("M-S-<Up>",               shiftToNext)
  , ("M-S-<Down>",             shiftToPrev)

----------------------------------------------------------------------------------------------------
-- Media Controls
  , ("<XF86AudioRaiseVolume>", spawn "volume.sh increase")
  , ("<XF86AudioLowerVolume>", spawn "volume.sh decrease")
  , ("<XF86AudioMute>",        spawn "volume.sh toggle-mute")
  , ("<XF86AudioMicMute>",     spawn "volume.sh toggle-mic-mute")
  , ("<XF86AudioPlay>",        spawn "playerctl play-pause")
  , ("<XF86AudioStop>",        spawn "playerctl pause")
  , ("<XF86AudioNext>",        spawn "playerctl next")
  , ("<XF86AudioPrev>",        spawn "playerctl previous")
  , ("M-S-s",                  spawn "toggle-sink.sh")

----------------------------------------------------------------------------------------------------
-- Scratchpad Controls
  , ("M-C-e",                  namedScratchpadAction myScratchpads "Emacs")
  , ("M-C-<Return>",           namedScratchpadAction myScratchpads "Terminal")
  , ("M-C-t",                  namedScratchpadAction myScratchpads "Terminal")
  , ("M-C-k",                  namedScratchpadAction myScratchpads "KeePassXC")
  , ("M-C-s",                  namedScratchpadAction myScratchpads "Spotify")
  , ("M-C-v",                  namedScratchpadAction myScratchpads "Discord")
  , ("<XF86Messenger>",        namedScratchpadAction myScratchpads "Discord")
  ]

----------------------------------------------------------------------------------------------------
-- MOUSEBINDINGS
myMouseBindings :: XConfig Layout -> M.Map (KeyMask, Button) (Window -> X ())
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $
  [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w >> windows W.shiftMaster))
  , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))
  , ((modm, button3), (\w -> focus w >> mouseResizeWindow w >> windows W.shiftMaster))
  ]

----------------------------------------------------------------------------------------------------
-- LAYOUTS
myLayout = toggleLayouts (noBorders Full) (avoidStruts $ spacingWithEdge spacing $ tiled ||| Mirror tiled ||| Grid ||| spiral spiralRatio)
  where
    tiled = Tall nmaster delta ratio
    nmaster = 1
    ratio = 1 / 2
    delta = 3 / 100
    spiralRatio = 6 / 7
    spacing = 3

----------------------------------------------------------------------------------------------------
-- MANAGE HOOK
myManageHook :: ManageHook
myManageHook =
  composeAll
    [ className =? "Gimp" --> doFloat
    , isDialog            --> doFloat
    ] <+> namedScratchpadManageHook myScratchpads

----------------------------------------------------------------------------------------------------
-- EVENT HANDLING
myEventHook :: Event -> X All
myEventHook = mempty

----------------------------------------------------------------------------------------------------
-- LOG HOOK
myLogHook :: X ()
myLogHook = return ()

----------------------------------------------------------------------------------------------------
-- STARTUP HOOK
myStartupHook :: X ()
myStartupHook = do
  spawnOnce "picom --daemon"
  spawnOnce "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1"
  spawnOnce "xfce4-power-manager --daemon"
  spawnOnce "thunar --daemon"
  spawnOnce "gebaard -b"
  spawnOnce "xsetroot -xcf /usr/share/icons/Adwaita/cursors/left_ptr 18"
  spawnOnce "autorun.sh"
  spawn "screen-layout.sh; polybar.sh & \
         \ feh --bg-fill $HOME/Pictures/Backgrounds/background.png"
  windows $ greedyViewOnScreen 0 "2"
  windows $ greedyViewOnScreen 1 "3"
  windows $ greedyViewOnScreen 2 "4"
  windows $ greedyViewOnScreen 3 "1"

----------------------------------------------------------------------------------------------------
-- XMONAD

main :: IO ()
main = xmonad
       . ewmh
       . addEwmhWorkspaceSort (pure (filterOutWs [scratchpadWorkspaceTag]))
       . docks
       $ myConfig

myConfig =
  def
    {
      terminal           = myTerminal,
      focusFollowsMouse  = myFocusFollowsMouse,
      clickJustFocuses   = myClickJustFocuses,
      borderWidth        = myBorderWidth,
      modMask            = myModMask,
      workspaces         = myWorkspaces,
      normalBorderColor  = myNormalBorderColor,
      focusedBorderColor = myFocusedBorderColor,

      keys               = myKeys,
      mouseBindings      = myMouseBindings,
      layoutHook         = myLayout,
      manageHook         = myManageHook,
      handleEventHook    = myEventHook,
      logHook            = myLogHook,
      startupHook        = myStartupHook
    }
