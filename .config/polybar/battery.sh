#!/usr/bin/env bash

battery_path="/sys/class/power_supply/BAT0"

battery_level=0
battery_max=0
battery_percent="X"

if [ -f "$battery_path/energy_now" ]; then
    battery_level=$(cat "$battery_path/energy_now")
fi

if [ -f "$battery_path/energy_full" ]; then
    battery_max=$(cat "$battery_path/energy_full")
    battery_percent=$(("$battery_level * 100"))
    battery_percent=$(("$battery_percent / $battery_max"))
fi

echo "$battery_percent%"
