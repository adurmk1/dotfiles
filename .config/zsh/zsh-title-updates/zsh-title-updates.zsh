function update_title() {
  local a
  a=${(V)1//\%/\%\%}
  print -nz "%20>...>$a"
  read -rz a
  a=${a//$'\n'/}
  print -n "\e]0;${(%)a}\a"
}

function _zsh_title__precmd() {
  update_title "zsh" "%20<...<%~"
}

function _zsh_title__preexec() {
  local -a cmd
  1=${1//\\/\\\\\\\\}
  cmd=(${(z)1})
  case $cmd[1] in
    fg)	cmd="${(z)jobtexts[${(Q)cmd[2]:-%+}]}" ;;
    %*)	cmd="${(z)jobtexts[${(Q)cmd[1]:-%+}]}" ;;
  esac
  update_title "$cmd" "%20<...<%~"
}

autoload -Uz add-zsh-hook
add-zsh-hook precmd _zsh_title__precmd
add-zsh-hook preexec _zsh_title__preexec
