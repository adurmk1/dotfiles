# ENVIRONMENT VARIABLES
# Programs
export EDITOR='emacs -nw'
export VISUAL="emacsclient -c -a 'emacs'"
export PAGER='bat'
export BAT_PAGER='less'
export MANROFFOPT='-c'
export MANPAGER="sh -c 'col -bx | bat --paging=always -l man -p'"

# Starship
export STARSHIP_CONFIG="$HOME/.config/starship/starship.toml"

# ZOXIDE
eval "$(zoxide init --cmd cd bash)"

# STARSHIP
eval "$(starship init bash)"

# STARTUP
fastfetch

# ALIASES
# Emacs
alias em='emacs -nw'
alias ec="emacsclient -nw -a 'emacs -nw'"
alias emacs-restart='systemctl --user restart emacs.service'

# Ls
alias ls='eza'
alias l='eza -lAh'
alias lh='eza -lh'

# Dirs
alias md='mkdir'
alias rd='rmdir'

# Grep
alias grep='grep --color=auto'

# Pacman
alias pac='sudo pacman'

# Objdump
alias odump='objdump -Matt --disassembler-color=on -d'

# Vallgrind
alias callgrind='valgrind --tool=callgrind '
alias cachegrind='valgrind --tool=cachegrdind'

# Ttyper
alias ttypere='ttyper -l english1000'
alias ttyperc='ttyper -l c'
